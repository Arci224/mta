FROM debian:stable-slim

RUN apt-get update && apt-get install -y \
	unzip \
	wget \
	bash \
	git \
	default-libmysqlclient-dev \
	libncurses5-dev \
	libncursesw5-dev \
	libncurses5 \
	libncursesw5 \
	&& apt clean \
	&& ldconfig

RUN wget -O libmysqlclient.so.16 https://nightly.mtasa.com/files/modules/64/libmysqlclient.so.16
RUN mv libmysqlclient.so.16 /usr/lib/x86_64-linux-gnu/

USER container
ENV  USER container
ENV  HOME /home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
